<?php
/*
Plugin Name: LG Woocommerce Ajax Cart
Plugin URI: https://www.longevitygraphics.com/
description: A custom plugin to fetch Woocommerce mini cart
Version: 1.0.0
Author: Gurpreet Dhanju
Author URI: https://www.longevitygraphics.com/
*/


add_action( 'wp_enqueue_scripts', 'lg_ajax_cart_js' );
/**
 * Enqueue Swiperjs when needed
 */
function lg_ajax_cart_js() {
	// JavaScript.
	wp_enqueue_script(
		'script-js',
		plugins_url( 'script.js', __FILE__ ),
		array( 'jquery' ),
		filemtime( plugin_dir_path( __FILE__ ) . 'script.js' ),
		true
	);

	// Pass ajax_url to script.js.
	wp_localize_script(
		'script-js',
		'lg_ajax_cart_object',
		array( 'ajax_url' => admin_url( 'admin-ajax.php' ) )
	);
}

/* AJAX request */
add_action( 'wp_ajax_lgWooCart', 'lg_woo_cart_callback' );
add_action( 'wp_ajax_nopriv_lgWooCart', 'lg_woo_cart_callback' );

/**
 * Get mini cart
 */
function lg_woo_cart_callback() {
	$html = '';
	if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {
		$count = WC()->cart->cart_contents_count;
		if ( $count > 0 ) {
			$html = '<a class="cart-contents" href="' . WC()->cart->get_cart_url() . '" title="View your shopping cart"><span class="cart-contents-count">' . esc_html( $count ) . '</span>' . WC()->cart->get_cart_total() . '<div class="viewcart">View Cart</div></a>';
		}
	}
	$response = array( 'html' => $html );
	echo wp_json_encode( $response );
	wp_die();
}
