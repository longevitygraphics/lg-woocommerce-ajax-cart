jQuery(document).ready(function ($) {
  var topCharts = $(".top-carts");
  if (topCharts && topCharts.length) {

    // AJAX url
    var ajax_url = lg_ajax_cart_object.ajax_url;

    // Fetch All records (AJAX request without parameter)
    var data = {
      'action': 'lgWooCart'
    };

    $.ajax({
      url: ajax_url,
      type: 'post',
      data: data,
      dataType: 'json',
      success: function (response) {
        if (response) {
          topCharts.html(response.html);
        }
      }
    });
  }
});
